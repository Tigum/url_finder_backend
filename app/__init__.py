# src/app.py

import os

from flask import Flask
from flask_cors import CORS

from .config import app_config
from .models import db

from .views.UrlView import url_api as url_blueprint
from .views.LinkView import link_api as link_blueprint


def create_app(env_name=os.getenv("FLASK_ENV", "production")):
    # app initiliazation
    app = Flask(__name__)

    if env_name == "development":
        CORS(app, resources={r"/*": {"origins": "*"}})

    app.config.from_object(app_config[env_name])

    # initializing bcrypt and db
    db.init_app(app)

    app.register_blueprint(url_blueprint, url_prefix="/api/v1/url")
    app.register_blueprint(link_blueprint, url_prefix="/api/v1/link")

    return app
