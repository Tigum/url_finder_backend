import unittest
import os
import json
from app import create_app, db
from ..utils.CustomResponse import ERROR_CODE, SUCCESS_CODE, CREATION_CODE


class LinksTest(unittest.TestCase):
    def setUp(self):

        self.app = create_app("testing")
        self.client = self.app.test_client
        self.create_url = {
            "url": "http://www.globo.com",
        }

        with self.app.app_context():
            # create all tables
            db.create_all()

    def test_link_fetching_by_url(self):
        """ test fetching links by url address """

        res = self.client().post(
            "/api/v1/url/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_url),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        res = self.client().get(
            "/api/v1/link/?url={}".format(self.create_url["url"]),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, SUCCESS_CODE)

    def test_link_fetching_by_url_fail_with_no_url_provided(self):
        """ test fetching links by url address - fail with no url provided"""

        res = self.client().post(
            "/api/v1/url/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_url),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        res = self.client().get(
            "/api/v1/link/?url={}".format(""),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "URL not provided.")
        self.assertEqual(res.status_code, ERROR_CODE)

    def test_link_fetching_by_url_fail_with_no_url_provided(self):
        """ test fetching links by url address - fail with no url provided"""

        res = self.client().post(
            "/api/v1/url/",
            headers={"Content-Type": "application/json"},
            data=json.dumps(self.create_url),
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "success")
        self.assertEqual(res.status_code, CREATION_CODE)

        res = self.client().get(
            "/api/v1/link/?url={}".format("www.google.com"),
            headers={"Content-Type": "application/json"},
        )
        json_data = json.loads(res.data)
        self.assertEqual(json_data["msg"], "URL does not exist in the DB.")
        self.assertEqual(res.status_code, ERROR_CODE)

    def tearDown(self):
        """ Delete all tables"""
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == "__main__":
    unittest.main()
