import httplib2
from flask import request, json, Response, Blueprint, g
from ..models.UrlModel import UrlModel, url_schema
from ..models.LinkModel import LinkModel, link_schema
from bs4 import BeautifulSoup, SoupStrainer
from ..utils.CustomResponse import (
    CREATION_CODE,
    ERROR_CODE,
    custom_response,
    DELETED_CODE,
    SUCCESS_CODE,
)

url_api = Blueprint("url_api", __name__)
http = httplib2.Http()


@url_api.route("/", methods=["POST"])
def create():
    req_data = request.get_json()
    url_address = req_data.get("url")

    if not url_address:
        return custom_response("Please provide url.", [], ERROR_CODE)

    url_exists = UrlModel.get_url_by_url(url_address)
    if url_exists:
        url_exists.delete()

    try:
        status, response = http.request(url_address)
        links = []
        for link in BeautifulSoup(response, parse_only=SoupStrainer("a")):
            if link.has_attr("href"):
                if link["href"][0:4] == "http":
                    links.append(link["href"])

        if len(links) > 0:
            url_load = url_schema.load(req_data)
            url = UrlModel(url_load)
            url.save()

            saved_url_info = url_schema.dump(url)
            url_id = saved_url_info["id"]

            for link in links:
                link_data = link_schema.load({"link": link, "url": url_id})
                link = LinkModel(link_data)
                link.save()

            return custom_response(
                "success", {"url": saved_url_info, "links": links}, CREATION_CODE
            )
        return custom_response("No links found", [], CREATION_CODE)
    except:
        return custom_response("URL Provided is invalid", [], ERROR_CODE)


@url_api.route("/", methods=["DELETE"])
def delete():
    url_id = request.args.get("id")

    if not url_id:
        return custom_response("URL ID not provided.", [], ERROR_CODE)

    url_exists = UrlModel.get_one_url(url_id)
    if url_exists:
        url_exists.delete()
        return custom_response(
            "URL and its links deleted successfully", None, DELETED_CODE
        )
    return custom_response("URL does not exist.", [], ERROR_CODE)


@url_api.route("/", methods=["GET"])
def fetch_urls():
    fetched_urls = UrlModel.get_all_urls()
    urls = []
    for fetched_url in fetched_urls:
        url = url_schema.dump(fetched_url)
        urls.append(url)
    return custom_response("success", urls, SUCCESS_CODE)